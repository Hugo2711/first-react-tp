import React, { useState, useEffect } from "react";
import axios from "axios";

function CardChuckNorris() {
  const [cardId, setCardId] = useState("");
  const [cardValue, setCardValue] = useState("");
  var images = [],
    index = 0;
  images[0] =
    "https://fr.web.img2.acsta.net/r_1920_1080/pictures/17/08/16/15/54/182780.jpg";
  images[1] =
    "https://fr.web.img3.acsta.net/r_1920_1080/medias/nmedia/18/72/99/62/19185872.jpg";
  images[2] =
    "https://fr.web.img3.acsta.net/r_1920_1080/medias/nmedia/18/36/31/21/18470550.jpg";
  images[3] =
    "https://fr.web.img2.acsta.net/r_1920_1080/pictures/20/03/19/10/25/0297689.jpg";
  images[4] =
    "https://fr.web.img6.acsta.net/r_1920_1080/medias/nmedia/18/35/81/93/18441011.jpg";
  images[5] =
    "https://fr.web.img6.acsta.net/r_1920_1080/medias/nmedia/18/35/81/93/18441008.jpg";
  images[6] =
    "https://fr.web.img6.acsta.net/r_1920_1080/medias/nmedia/18/35/81/93/18441009.jpg";
  images[7] =
    "https://fr.web.img2.acsta.net/r_1920_1080/medias/nmedia/18/35/81/93/18441004.jpg";
  images[8] =
    "https://fr.web.img4.acsta.net/r_1920_1080/medias/nmedia/18/35/81/93/18441006.jpg";
  images[9] =
    "https://fr.web.img3.acsta.net/r_1920_1080/medias/nmedia/18/35/81/93/18441014.jpg";
  index = Math.floor(Math.random() * images.length);
  const cardData = async () => {
    try {
      const res = await axios.get("https://api.chucknorris.io/jokes/random");
      console.log(res);
      setCardId(res.data.id);
      setCardValue(res.data.value);
    } catch (error) {
      console.log(error);
    }
  };
  useEffect(() => {
    cardData();
  }, []);

  return (
        <div class="p-4 md:w-1/3 md:mb-0 mb-6 flex flex-col justify-center items-center max-w-sm mx-auto">
          <div
            class="bg-gray-300 h-56 w-full rounded-lg shadow-md bg-cover bg-center"
            style={{ backgroundImage: `url(${images[index]})` }}
          ></div>

          <div class=" w-70 bg-white -mt-10 shadow-lg rounded-lg overflow-hidden p-5">
            <div class="title-post font-medium">ID : {cardId}</div>
            <div class="summary-post text-base text-justify">{cardValue} </div>
          </div>
        </div>
  );
}

export default CardChuckNorris;
