import React from "react";
import CardChuckNorris from "../components/cardChuckNorris";

function Main() {
  function refreshPage() {
    window.location.reload();
  }
  return (
    <section className="text-gray-700 blog body-font">
      <div className="container px-5 py-24 mx-auto">
      <div className="flex flex-col flex-wrap items-center w-full mb-20 text-center">
          <h1 className="mb-2 text-2xl font-medium text-gray-900 sm:text-3xl title-font"> Chuck Norris - Fun facts</h1>
          <button className="px-3 py-2 text-green-400 border-2 border-green-600 rounded-lg cursor-pointer hover:bg-green-600 hover:text-green-200"onClick={refreshPage}>Get new fact</button>
        </div>
        <div className="flex flex-wrap -mx-4 -mt-4 -mb-10 sm:-m-4">
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
          <CardChuckNorris />
        </div>
      </div>
    </section>
  );
}

export default Main;
